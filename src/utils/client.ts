import Pocket from '@natade-coco/pocket-sdk'
import { SDK } from '@natade-coco/hub-sdk'
import { TxForm } from '@natade-coco/hub-sdk/dist/Type'

export { SDK } from '@natade-coco/hub-sdk'

const IdentityHubService = 'IdentityHubService'
const PaymentHubService = 'PaymentHubService'

export interface SDKOpts {
  test?: boolean
  identityHubToken?: string
  paymentHubToken?: string
}

export class NatadeCOCO {
  private client: SDK

  constructor(private opts?: SDKOpts) {}

  async loadSDK() {
    let jwt = ''
    if (this.opts?.identityHubToken) {
      jwt = this.opts?.identityHubToken
    } else {
      jwt = await Pocket.requestSignJWT(IdentityHubService)
    }
    let test = false
    if (this.opts?.test) {
      test = this.opts?.test
    }
    const c = await SDK.initWithJWT({ jwt: jwt, test: test })
    this.client = c
  }

  async GetPaymentTx(tx: string) {
    if (this.opts?.paymentHubToken) {
      return this.client.GetPaymentTx(this.opts?.paymentHubToken, tx)
    }
    const jwt = await Pocket.requestSignJWT(PaymentHubService)
    return this.client.GetPaymentTx(jwt, tx)
  }

  async NewPaymentTx(form: TxForm) {
    if (this.opts?.paymentHubToken) {
      return this.client.NewPaymentTx(this.opts?.paymentHubToken, form)
    }
    const jwt = await Pocket.requestSignJWT(PaymentHubService)
    return this.client.NewPaymentTx(jwt, form)
  }

  async DelPaymentTx(tx: string) {
    if (this.opts?.paymentHubToken) {
      return this.client.DelPaymentTx(this.opts?.paymentHubToken, tx)
    }
    const jwt = await Pocket.requestSignJWT(PaymentHubService)
    return this.client.DelPaymentTx(jwt, tx)
  }
}
