import React, { useEffect, useState } from 'react'

import { NatadeCOCO, SDKOpts } from '../utils/client'
import { ReactComponent as IconNormal } from '../images/icon_cocopay.svg'
import { ReactComponent as IconWhite } from '../images/icon_cocopay_white.svg'
import { ReactComponent as IconDark } from '../images/icon_cocopay_dark.svg'
import '@fontsource/m-plus-1p/800.css'
import css from './Payout.module.css'

const SESSION_KEY = 'tx'
const STATUS_SUCCEEDED = 'succeeded'
const TARGET = '_self'

interface PayoutProps {
  theme?: string // light, dark, inverted
  layout?: string // default, short, stack
  style?: React.CSSProperties
  account: string
  amount: number
  onCompleted: (txId: string, status: string, chargeId: string, receiptUrl: string) => boolean
  onError: (reason: any) => void
  opts?: SDKOpts
}

const Payout = (props: PayoutProps) => {
  const [client, setClient] = useState<NatadeCOCO>()
  // @ts-ignore TS6133: 'setLocation' is declared but its value is never read.
  const [location, setLocation] = useState(new URL(window.location.href))
  // @ts-ignore TS6133: 'setTx' is declared but its value is never read.
  const [tx, setTx] = useState(window.localStorage.getItem(SESSION_KEY))
  const [disabled, setDisabled] = useState(true)
  const [completed, setCompleted] = useState(false)

  useEffect(() => {
    confirmState()
  }, [])

  const confirmState = async () => {
    // セットアップ
    const client = new NatadeCOCO(props.opts)
    await client.loadSDK()
    setClient(client)

    if (tx) {
      // セッションあり(復帰)
      // トランザクション取得とローカルストレージからの除去
      const result = await client
        .GetPaymentTx(tx)
        .catch((reason) => {
          props.onError(reason)
        })
        .finally(() => {
          window.localStorage.removeItem(SESSION_KEY)
        })
      // トランザクションの状態チェック
      if (result && result.transaction.status === STATUS_SUCCEEDED) {
        // 支払い成功
        setDisabled(false)
        setCompleted(true)
        const confirm = props.onCompleted(result.transaction.id, result.transaction.status, result.transaction.charge_id, result.transaction.receipt_url)
        if (confirm === true) {
          setCompleted(false)
        }
      } else {
        // 支払い失敗
        setDisabled(false)
      }
    } else {
      // セッションなし(新規)
      setDisabled(false)
    }
  }

  const handleOnClick = () => {
    if (client && !disabled) {
      setDisabled(true)
      client
        .NewPaymentTx({
          amount: Number(props.amount),
          account_id: props.account,
          redirect_url: location.href
        })
        .then((result) => {
          // 復帰時処理のためセッションストレージに保存
          window.localStorage.setItem(SESSION_KEY, result.transaction.id)
          // Payアプリを開く
          window.open(result.transaction.url, TARGET)
        })
        .catch((reason) => {
          props.onError(reason)
        })
    }
  }

  return (
    <Button
      theme={props.theme}
      layout={props.layout}
      style={props.style}
      loading={disabled}
      completed={completed}
      handleOnClick={handleOnClick}
    />
  )
}

interface ButtonProps {
  loading?: boolean
  completed?: boolean
  theme?: string // light, dark, inverted
  layout?: string // default, short, stack
  style?: React.CSSProperties
  handleOnClick: () => void
}

const ButtonExplain = (props: { layout?: string; completed?: boolean }) => {
  if (props.completed) {
    return (
      <div className={`${css.explain}`}>
        <span>購入済み</span>
      </div>
    )
  }
  if (props.layout === 'short') {
    return (
      <div className={`${css.explain}`}>
        <span>お支払い</span>
      </div>
    )
  }
  if (props.layout === 'stack') {
    return (
      <div className={`${css.explain} ${css.stack}`}>
        <span>ココペイ</span>
        <span style={{ fontSize: '0.5rem' }}>でお支払い</span>
      </div>
    )
  }
  // default: normal
  return (
    <div className={`${css.explain}`}>
      <span>ココペイ</span>
      <span>でお支払い</span>
    </div>
  )
}

const ButtonIcon = (props: { theme?: string; loading?: boolean }) => {
  if (props.theme === 'dark') {
    return (
      <IconNormal
        className={`${css.icon} ${props.loading ? css.hiddensvg : ''}`}
      />
    )
  }
  if (props.theme === 'inverted') {
    return (
      <IconDark
        className={`${css.icon} ${props.loading ? css.hiddensvg : ''}`}
      />
    )
  }
  // default: light
  return (
    <IconWhite
      className={`${css.icon} ${props.loading ? css.hiddensvg : ''}`}
    />
  )
}

const Button = (props: ButtonProps) => {
  let themeClass
  if (props.theme === 'dark') {
    themeClass = css['is-dark']
  } else if (props.theme === 'inverted') {
    themeClass = css['is-inverted']
  } else {
    themeClass = css['is-light']
  }

  let layoutClass
  if (props.layout === 'short') {
    layoutClass = css['is-short']
  } else if (props.layout === 'stack') {
    layoutClass = css['is-stack']
  } else {
    layoutClass = css['is-basic']
  }

  return (
    <button
      style={props.style}
      className={`${css.button} ${layoutClass} ${themeClass} ${
        props.loading ? css['is-loading'] : ''
      }`}
      disabled={props.completed}
      onClick={() => props.handleOnClick()}
    >
      <ButtonIcon theme={props.theme} loading={props.loading} />
      <ButtonExplain layout={props.layout} completed={props.completed} />
    </button>
  )
}

export default Payout
