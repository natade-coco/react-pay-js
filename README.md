# @natade-coco/react-pay-js

> Library for COCOPay integration

[![NPM](https://img.shields.io/npm/v/@natade-coco/react-pay-js.svg)](https://www.npmjs.com/package/@natade-coco/react-pay-js) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## 🚧 Release Candidate

This is _pre-release_ software. Changes may occur at any time, and
certain features might be missing or broken. 

## 📌 Requirements

You have to enable Stripe connected account via [natadeCOCO Console](https://console.natade-coco.com/) and get your account ID.

## ⚙️ Install

```bash
npm install --save @natade-coco/react-pay-js
```

## 🚀 Usage

```tsx
import React, { Component } from 'react'

import { Payout } from '@natade-coco/react-pay-js'
import '@natade-coco/react-pay-js/dist/index.css'

class Example extends Component {
  const handleOnCompleted = (txId: string, status: string, chargeId: string, receiptUrl: string): boolean => {
    // callback after payment success
    return true
  }
  const handleOnError = (reason: any) => {
    // callback when an error occurred
  }
  render() {
    return <Payout
      theme='light' // option: light(default), dark, inverted
      layout='basic' // option: basic(default), short, stack 
      account='YOUR_ACCOUNT_ID'
      amount={1000} 
      onCompleted={handleOnCompleted}
      onError={handleOnError} />
  }
}
```

## 🍭 Layouts & Colors

|       |                            light                             |                            dark                             |                            inverted                             |
| :---: | :----------------------------------------------------------: | :---------------------------------------------------------: | :-------------------------------------------------------------: |
| basic | <img src="./example/public/img/basic-light.png" width="300"> | <img src="./example/public/img/basic-dark.png" width="300"> | <img src="./example/public/img/basic-inverted.png" width="300"> |
| short | <img src="./example/public/img/short-light.png" width="160"> | <img src="./example/public/img/short-dark.png" width="160"> | <img src="./example/public/img/short-inverted.png" width="160"> |
| stack | <img src="./example/public/img/stack-light.png" width="140"> | <img src="./example/public/img/stack-dark.png" width="140"> | <img src="./example/public/img/stack-inverted.png" width="140"> |


## License

MIT © [natadeCOCO](https://natade-coco.com)
