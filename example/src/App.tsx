import React from 'react'

import item from './item.jpg'

import { Payout } from '@natade-coco/react-pay-js'
import '@natade-coco/react-pay-js/dist/index.css'

const App = () => {
  // read from example/.env
  const opts = {
    test: true,
    identityHubToken: process.env.REACT_APP_IDENTYTYHUB_JWT || '',
    paymentHubToken: process.env.REACT_APP_PAYMENTHUB_JWT || ''
  }
  const connectAcc = process.env.REACT_APP_CONNECT_ACCOUNT || ''
  const amount = 440

  const handleOnCompleted = (txId: string, status: string, chargeId: string, receiptUrl: string): boolean => {
    console.log(txId, status, chargeId, receiptUrl)
    return true
  }

  const handleOnError = (reason: any) => {
    console.log(reason)
  }
  
  return (
    <div style={{ height: '100vh', display: 'flex', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center' }}>
      <Example />
      <Payout
        theme='inverted' // option: light(default), dark, inverted
        layout='stack' // option: basic(default), short, stack 
        account={connectAcc}
        amount={amount}
        onCompleted={handleOnCompleted}
        onError={handleOnError}
        opts={opts}
      />
    </div>
  )
}

const Example = () => {
  return (
    <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center' }}>
      <div style={{ width: '90%', textAlign: 'center', fontWeight: 'bold', fontSize: '1.5rem' }}>モーニングセット</div>
      <img style={{ width: '90%', marginTop: '2rem' }} src={item} alt='samle' />
      <table style={{ width: '80%', marginTop: '2rem', textAlign: 'center' }}>
        <tbody>
          <tr>
            <td style={{ textAlign: 'right', width: '30%' }}>小計</td>
            <td>1点</td>
            <td>×</td>
            <td style={{ textAlign: 'right', width: '30%' }}>400円</td>
          </tr>
          <tr>
            <td style={{ textAlign: 'right', width: '30%' }}>消費税(10%)</td>
            <td></td>
            <td></td>
            <td style={{ textAlign: 'right', width: '30%' }}>40円</td>
          </tr>
          <tr style={{ fontSize: '1.5rem', fontWeight: 'bold' }}>
            <td style={{ textAlign: 'right', width: '30%' }}>合計</td>
            <td></td>
            <td></td>
            <td style={{ textAlign: 'right', width: '30%' }}>440円</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default App
